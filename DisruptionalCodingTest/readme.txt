I decided to write the test in C# and structure it as .NetStandard Class Libraries with corresponding unit test .Net Core Projects.
As my unit test driver I chose xUnit.
I wrote them using VisualStudio on Mac and tested it on Windows as well, but those tests should also run in VisualStudioCode.

Here is the file structure of the solution:

DisruptionalCodingTest/
|-- DisruptionalCodingTest.sln
|-- Geometry
|   |-- Geometry.csproj
|   |-- Models
|   |   |-- Point2D.cs
|   |   `-- TurnType.cs
|   `-- PointsHelper.cs
|-- GeometryTests
|   |-- GeometryTests.csproj
|   |-- Point2DTests.cs
|   `-- PointsHelperTests.cs
|-- Tournament
|   |-- Models
|   |   |-- Pairing.cs
|   |   `-- Round.cs
|   |-- Tournament.csproj
|   |-- TournamentGenerator.cs
|   `-- TournamentWriter.cs
|-- TournamentTests
|   |-- TournamentGeneratorTests.cs
|   |-- TournamentTests.csproj
|   `-- TournamentWriterTests.cs
`-- readme.txt

Given time limits on the coding exercise I have not implemented code that would take care of following special cases in Part A Question 5:
- situation when casted ray intersect polygon through a vertex
- situation when casted ray is collinear with one of polygons segment