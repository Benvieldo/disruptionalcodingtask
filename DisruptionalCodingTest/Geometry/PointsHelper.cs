﻿using System;
using Geometry.Models;

namespace Geometry
{
    public static class PointsHelper
    {
        public static Point2D[] SortPoints(Point2D[] pointsToSort)
        {
            Point2D[] clonedPointsToSort = (Point2D[]) pointsToSort.Clone();
            Array.Sort(clonedPointsToSort, (point1, point2) =>
            {
                return point1.DistanceFromOrigin().CompareTo(point2.DistanceFromOrigin());
            });
            return clonedPointsToSort;
        }

        public static bool PointsFormAntiClockwiseTurn(Point2D point1, Point2D point2, Point2D point3)
        {
            return GetTurnType(point1, point2, point3) == TurnType.AntiClockwise;
        }

        public static bool PointsAreCollinear(Point2D point1, Point2D point2, Point2D point3)
        {
            return GetTurnType(point1, point2, point3) == TurnType.Collinear;
        }

        public static bool LineSegmentsIntersect(Point2D p1, Point2D q1, Point2D p2, Point2D q2)
        {
            if (PointsAreCollinear(p1, q1, p2) &&
                PointsAreCollinear(p1, q1, q2) &&
                PointsAreCollinear(p2, q2, p1) &&
                PointsAreCollinear(p2, q2, q1))
            {
                return ProjectionsIntersect(p1.X, q1.X, p2.X, q2.X) && ProjectionsIntersect(p1.Y, q1.Y, p2.Y, q2.Y);
            }

            return GetTurnType(p1, q1, p2) != GetTurnType(p1, q1, q2) && GetTurnType(p2, q2, p1) != GetTurnType(p2, q2, q1);
        }

        private static bool ProjectionsIntersect(int p1, int q1, int p2, int q2)
        {
            return !((p2 < p1 && p2 < q1 && q2 < p1 && q2 < q1) ||
                (p2 > p1 && p2 > q1 && q2 > p1 && q2 > q1));
        }

        private static TurnType GetTurnType(Point2D point1, Point2D point2, Point2D point3)
        {
            var twiceSignedArea = (point2.X - point1.X) * (point3.Y - point1.Y) - (point2.Y - point1.Y) * (point3.X - point1.X);
            if (twiceSignedArea == 0)
            {
                return TurnType.Collinear;
            }
            if (twiceSignedArea > 0)
            {
                return TurnType.AntiClockwise;
            }

            return TurnType.Clockwise;
        }
    }
}
