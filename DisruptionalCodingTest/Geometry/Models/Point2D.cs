﻿using System;
using System.Linq;
namespace Geometry.Models
{
    public class Point2D
    {
        public int X { get; }
        public int Y { get; }

        public Point2D(int x, int y)
        {
            X = x;
            Y = y;
        }

        public double GetDistance(Point2D secondPoint)
        {
            return Math.Sqrt(Math.Pow(secondPoint.X - X, 2) + Math.Pow(secondPoint.Y - Y, 2));
        }

        public double DistanceFromOrigin()
        {
            return GetDistance(new Point2D(0, 0));
        }

        public bool LiesInsidePolygon(Point2D[] polygon)
        {
            var maxY = polygon.Max(p => p.Y);
            var pointOutsidePolygon = new Point2D(X, maxY);
            var intersections = 0;

            for(int i = 0; i < polygon.Length - 1; i++)
            {
                if (!ArtificialLineCrossesThroughVertex(polygon[i], pointOutsidePolygon))
                {
                    if (PointsHelper.LineSegmentsIntersect(polygon[i], polygon[i + 1], this, pointOutsidePolygon))
                    {
                        intersections++;
                    }
                }
            }

            if (!ArtificialLineCrossesThroughVertex(polygon.Last(), pointOutsidePolygon))
            {
                if (PointsHelper.LineSegmentsIntersect(polygon[0], polygon.Last(), this, pointOutsidePolygon))
                {
                    intersections++;
                }
            }

            return intersections > 0 && intersections % 2 == 1;
        }

        private bool ArtificialLineCrossesThroughVertex(Point2D vertex, Point2D pointOutsidePolygon)
        {
            return vertex.X == X && vertex.Y >= Math.Min(Y, pointOutsidePolygon.Y) && vertex.Y <= Math.Max(Y, pointOutsidePolygon.Y);
        }

        public override bool Equals(object obj)
        {
            var point2 = obj as Point2D;
            if(point2 == null)
            {
                return false;
            }
            return X == point2.X && Y == point2.Y;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(X, Y);
        }
    }
}
