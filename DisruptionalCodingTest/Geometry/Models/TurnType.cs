﻿namespace Geometry.Models
{
    public enum TurnType
    {
        AntiClockwise,
        Clockwise,
        Collinear
    }
}
