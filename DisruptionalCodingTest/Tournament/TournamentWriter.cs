﻿using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Tournament
{
    public class TournamentWriter
    {
        public static void GenerateTournamentFiles(string destinationFolder, List<string> players)
        {
            var tournamentRounds = TournamentGenerator.GenerateRoundRobinPairings(players, 2);
            for (int i = 0; i < tournamentRounds.Count(); i++)
            {
                var fileName = $"Round{i + 1}Pairings.txt";
                using (StreamWriter outputFile = new StreamWriter(Path.Combine(destinationFolder, fileName)))
                {
                    outputFile.WriteLine(tournamentRounds[i]);
                }
            }
        }
    }
}
