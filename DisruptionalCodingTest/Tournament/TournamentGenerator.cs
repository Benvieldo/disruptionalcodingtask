﻿using System.Collections.Generic;
using System.Linq;
using Tournament.Models;

namespace Tournament
{
    public static class TournamentGenerator
    {
        public static List<Round> GenerateRoundRobinPairings(List<string> players, int numberOfPlays)
        {
            int playersCount = players.Count();
            if (playersCount % 2 == 1)
            {
                players.Add("BYE");
                playersCount++;
            }

            var fixedPlayer = players.First();
            int pairsCount = playersCount / 2;
            var firstRow = new LinkedList<string>(players.GetRange(1, pairsCount - 1));
            var secondRow = new LinkedList<string>(players.GetRange(pairsCount, pairsCount));

            var rounds = (playersCount - 1) * numberOfPlays;
            var tournamentRounds = new List<Round>();
            for (int i = 0; i < rounds; i++)
            {
                var round = PrepareRound(playersCount, fixedPlayer, firstRow, secondRow, i);
                tournamentRounds.Add(round);

                RotateRows(firstRow, secondRow);
            }

            return tournamentRounds;
        }

        private static Round PrepareRound(int playersCount, string fixedPlayer, LinkedList<string> firstRow, LinkedList<string> secondRow, int roundNumber)
        {
            var round = new Round();
            var firstRowEnumerator = firstRow.GetEnumerator();
            var secondRowEnumerator = secondRow.GetEnumerator();
            firstRowEnumerator.MoveNext();
            secondRowEnumerator.MoveNext();

            for (int j = 0; j < playersCount / 2 - 1; j++)
            {
                string playerFromFirstRow = firstRowEnumerator.Current;
                string playerFromSecondRow = secondRowEnumerator.Current;
                Pairing newPairing = PreparePairing(roundNumber, playerFromFirstRow, playerFromSecondRow);

                round.AddPairing(newPairing);
                firstRowEnumerator.MoveNext();
                secondRowEnumerator.MoveNext();
            }

            string lastPlayerFromSecondRow = secondRowEnumerator.Current;
            var fixedPlayerPairing = PreparePairing(roundNumber, fixedPlayer, lastPlayerFromSecondRow);
            round.AddPairing(fixedPlayerPairing);

            return round;
        }

        private static Pairing PreparePairing(int roundNumber, string firstPlayer, string secondPlayer)
        {
            if (roundNumber % 2 == 0)
            {
                return new Pairing(firstPlayer, secondPlayer);
            }

            return new Pairing(secondPlayer, firstPlayer);
        }

        private static void RotateRows(LinkedList<string> firstRow, LinkedList<string> secondRow)
        {
            var firstRowLastItem = firstRow.Last();
            firstRow.RemoveLast();
            secondRow.AddLast(firstRowLastItem);

            var secondRowFirstItem = secondRow.First();
            secondRow.RemoveFirst();
            firstRow.AddFirst(secondRowFirstItem);
        }
    }
}
