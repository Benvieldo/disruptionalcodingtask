﻿namespace Tournament.Models
{
    public class Pairing
    {
        public string playerA { get; }
        public string playerB { get; }

        public Pairing(string a, string b)
        {
            playerA = a;
            playerB = b;
        }
        public override string ToString()
        {
            return $"{playerA} -> {playerB}";
        }
    }
}
