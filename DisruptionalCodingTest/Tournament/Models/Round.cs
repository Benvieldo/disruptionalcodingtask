﻿using System.Collections.Generic;
using System.Text;

namespace Tournament.Models
{
    public class Round
    {
        public List<Pairing> pairings { get; }

        public Round()
        {
            pairings = new List<Pairing>();
        }

        public void AddPairing(Pairing newPairing)
        {
            pairings.Add(newPairing);
        }

        public override string ToString()
        {
            var result = new StringBuilder();
            pairings.ForEach(p => result.Append($"{p}\n"));
            return result.ToString();
        }
    }
}
