using System.Collections.Generic;
using System.Linq;
using Tournament;
using Xunit;
using System.IO;
using System;


namespace TournamentTests
{
    public class TournamentGeneratorTests
    {
        [Theory]
        [InlineData(6)]
        [InlineData(7)]
        [InlineData(20)]
        [InlineData(19)]
        public void GenerateRoundRobinPairingsShouldReturnProperTournament(int playersCount)
        {
            var players = new List<string>();
            for (var i = 1; i <= playersCount; i++)
            {
                players.Add(i.ToString());
            }

            var tournamentPairings = TournamentGenerator.GenerateRoundRobinPairings(players, 1);

            var allPairings = tournamentPairings.SelectMany(round => round.pairings).ToList();

            AssertAllPlayersPlayedWithEachOther(players, allPairings);
        }

        private static void AssertAllPlayersPlayedWithEachOther(List<string> players, List<Tournament.Models.Pairing> allPairings)
        {
            for (var i = 0; i < players.Count; i++)
            {
                for (var j = 0; j < players.Count; j++)
                {
                    if (i == j)
                        continue;

                    var containsPairing = allPairings.Any(p =>
                    (p.playerA == players[i] && p.playerB == players[j]) ||
                    (p.playerA == players[j] && p.playerB == players[i]));

                    Assert.True(containsPairing);
                }
            }
        }

        [Theory]
        [InlineData(6)]
        [InlineData(7)]
        [InlineData(20)]
        [InlineData(19)]
        public void GenerateRoundRobinPairingsShouldEnsureAllPlayersStartSameNumberOfTimes(int playersCount)
        {
            var players = new List<string>();
            for (var i = 1; i <= playersCount; i++)
            {
                players.Add(i.ToString());
            }

            var tournamentPairings = TournamentGenerator.GenerateRoundRobinPairings(players, 2);

            var allPairings = tournamentPairings.SelectMany(round => round.pairings).ToList();

            for (var i = 0; i < players.Count; i++)
            {
                for (var j = 0; j < players.Count; j++)
                {
                    if (i == j)
                        continue;

                    var containsPairingA = allPairings.Any(p =>
                    p.playerA == players[i] && p.playerB == players[j]);

                    Assert.True(containsPairingA);

                    var containsPairingB = allPairings.Any(p =>
                    p.playerA == players[j] && p.playerB == players[i]);

                    Assert.True(containsPairingB);
                }
            }
        }
    }
}
