﻿using System;
using System.Collections.Generic;
using System.IO;
using Tournament;
using Xunit;

namespace TournamentTests
{
    public class TournamentWriterTests
    {
        [Theory]
        [InlineData(6)]
        [InlineData(7)]
        [InlineData(20)]
        [InlineData(19)]
        public void GenerateTournamentFilesGeneratesCorrectAmountOfFiles(int playersCount)
        {
            var players = new List<string>();
            for (var i = 1; i <= playersCount; i++)
            {
                players.Add(i.ToString());
            }

            string desktopPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            var folderPath = Path.Combine(desktopPath, "TournamentPairings");
            Directory.CreateDirectory(folderPath);
            Array.ForEach(Directory.GetFiles(folderPath, "*.txt"), File.Delete);

            TournamentWriter.GenerateTournamentFiles(folderPath, players);

            var filesCount = Directory.GetFiles(folderPath, "*.txt").Length;
            Assert.Equal((players.Count - 1) * 2, filesCount);
        }
    }
}
