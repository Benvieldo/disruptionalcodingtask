﻿using System;
using Xunit;
using Geometry.Models;
using Geometry;

namespace GeometryTests
{
    public class PointsHelperTests
    {
        [Fact]
        public void SortPointsShouldProperlySortPoints()
        {
            var unsortedArray = new Point2D[] {
                new Point2D(3,3),
                new Point2D(1,1),
                new Point2D(3,2),
                new Point2D(2,3),
                new Point2D(9,9),
                new Point2D(1,1)
            };
            var expectedSortedArray = new Point2D[] {
                new Point2D(1,1),
                new Point2D(1,1),
                new Point2D(3,2),
                new Point2D(2,3),
                new Point2D(3,3),
                new Point2D(9,9)
            };

            var sortedArray = PointsHelper.SortPoints(unsortedArray);

            Assert.Equal(expectedSortedArray, sortedArray);
        }

        [Fact]
        public void SortPointsShouldNotEditOriginalArray()
        {
            var unsortedArray = new Point2D[] {
                new Point2D(3,3),
                new Point2D(1,1),
                new Point2D(3,2),
                new Point2D(2,3),
                new Point2D(9,9),
                new Point2D(1,1)
            };

            var expectedUnsortedArray = new Point2D[] {
                new Point2D(3,3),
                new Point2D(1,1),
                new Point2D(3,2),
                new Point2D(2,3),
                new Point2D(9,9),
                new Point2D(1,1)
            };

            _ = PointsHelper.SortPoints(unsortedArray);

            Assert.Equal(expectedUnsortedArray, unsortedArray);
        }

        [Theory]
        [InlineData(1, 1, 2, 2, 2, 3, true)]
        [InlineData(1, 1, 2, 2, 3, 3, false)]
        [InlineData(3, 1, 2, 2, 2, 3, false)]
        public void PointsFormAntiClockwiseTurnShouldReturnCorrectValue(int x1, int y1, int x2, int y2, int x3, int y3, bool expectedResult)
        {
            var point1 = new Point2D(x1, y1);
            var point2 = new Point2D(x2, y2);
            var point3 = new Point2D(x3, y3);

            var result = PointsHelper.PointsFormAntiClockwiseTurn(point1, point2, point3);

            Assert.Equal(expectedResult, result);
        }

        [Theory]
        [InlineData(1, 1, 2, 2, 2, 3, false)]
        [InlineData(1, 1, 2, 2, 3, 3, true)]
        [InlineData(3, 1, 2, 2, 2, 3, false)]
        public void PointsAreCollinearShouldReturnCorrectValue(int x1, int y1, int x2, int y2, int x3, int y3, bool expectedResult)
        {
            var point1 = new Point2D(x1, y1);
            var point2 = new Point2D(x2, y2);
            var point3 = new Point2D(x3, y3);

            var result = PointsHelper.PointsAreCollinear(point1, point2, point3);

            Assert.Equal(expectedResult, result);
        }

        [Theory]
        //intersecting segments
        [InlineData(1, 1, 4, 5, 2, 4, 4, 1, true)]
        [InlineData(1, 1, 4, 5, 4, 1, 2, 4, true)]
        //segments share one starting point
        [InlineData(1, 1, 4, 4, 1, 1, 1, 4, true)]
        [InlineData(4, 4, 1, 1, 1, 4, 1, 1, true)]
        [InlineData(1, 1, 4, 4, 1, 4, 1, 1, true)]
        //starting point of one segment lies on second segment
        [InlineData(1, 1, 4, 4, 3, 3, 5, 2, true)]
        [InlineData(1, 1, 4, 4, 5, 2, 3, 3, true)]
        //collinear segments
        [InlineData(1, 1, 1, 5, 1, 3, 1, 7, true)]
        [InlineData(1, 1, 1, 3, 1, 5, 1, 7, false)]
        [InlineData(1, 1, 1, 3, 1, 3, 1, 5, true)]
        //parallel non-intersecting segments
        [InlineData(1, 1, 5, 5, 2, 3, 3, 4, false)]
        [InlineData(1, 1, 5, 5, 3, 4, 2, 3, false)]

        public void LineSegmentsIntersectShouldReturnProperValues(int p1x, int p1y, int q1x, int q1y, int p2x, int p2y, int q2x, int q2y, bool expectedResult)
        {
            var p1 = new Point2D(p1x, p1y);
            var q1 = new Point2D(q1x, q1y);
            var p2 = new Point2D(p2x, p2y);
            var q2 = new Point2D(q2x, q2y);

            var result = PointsHelper.LineSegmentsIntersect(p1, q1, p2, q2);

            Assert.Equal(expectedResult, result);
        }
    }
}
