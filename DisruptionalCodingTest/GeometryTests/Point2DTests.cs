using System;
using Geometry.Models;
using Xunit;

namespace GeometryTests
{
    public class Point2DTests
    {
        [Theory]
        [InlineData(2, 2, 2, 4, 2.0)]
        [InlineData(2, 2, 4, 2, 2.0)]
        [InlineData(2, 2, 2, -4, 6.0)]
        [InlineData(2, 2, -4, 2, 6.0)]
        [InlineData(2, 2, 5, 6, 5.0)]
        [InlineData(2, 2, 2, 2, 0.0)]
        public void GetDistanceShouldReturnProperResult(int x1, int y1, int x2, int y2, double expectedResult)
        {
            var point1 = new Point2D(x1, y1);
            var point2 = new Point2D(x2, y2);

            var result = point1.GetDistance(point2);

            Assert.Equal(expectedResult, result);
        }

        [Theory]
        [InlineData(3, 3, PolygonType.SimpleConvex, true)]
        [InlineData(0, 0, PolygonType.SimpleConvex, false)]
        [InlineData(3, 0, PolygonType.SimpleConvex, false)]
        [InlineData(7, 3, PolygonType.SimpleConvex, false)]

        [InlineData(3, 2, PolygonType.ConcaveCrown, true)]
        [InlineData(4, 3, PolygonType.ConcaveCrown, false)]
        [InlineData(2, 2, PolygonType.ConcaveCrown, true)]

        public void LiesInsidePolygonShouldReturnCorrectValue(int x, int y, PolygonType polygonType, bool expectedResult)
        {
            var pointTested = new Point2D(x, y);
            var polygon = ConstructPolygon(polygonType);

            var result = pointTested.LiesInsidePolygon(polygon);

            Assert.Equal(expectedResult, result);
        }

        private Point2D[] ConstructPolygon(PolygonType type)
        {
            switch (type)
            {
                case PolygonType.SimpleConvex: // simple convex polygon
                    return new Point2D[]
                    {
                        new Point2D(1,1),
                        new Point2D(4,1),
                        new Point2D(6,3),
                        new Point2D(4,5),
                        new Point2D(2,4)
                    };
                case PolygonType.ConcaveCrown: // concave polygon (crown)
                    return new Point2D[]
                    {
                        new Point2D(1,1),
                        new Point2D(6,1),
                        new Point2D(4,4),
                        new Point2D(6,6),
                        new Point2D(2,6),
                    };
                default:
                    return null;
            }
        }

        public enum PolygonType
        {
            SimpleConvex,
            ConcaveCrown
        }
    }
}